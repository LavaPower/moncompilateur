			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1Ln:	.string "%llu\n"
FormatString2Ln:	.string "%c\n"
FormatString3Ln:	.string "%f\n"
FormatString1:	.string "%llu"
FormatString2:	.string "%c"
FormatString3:	.string "%f"
TrueString:	.string "True\n"
FalseString:	.string "False\n"
c:	.quad 0	# c - entier non signé
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $2
	pop c
For0:
	push $10
	pop %rdx
	cmp c, %rdx
	jb Fin0
	push c
	movq $FormatString1Ln, %rdi    # "%llu\n"
	pop %rsi                     # The value to be displayed
	movb    $0, %al
	subq $8, %rsp
	call    printf
	addq $8, %rsp
	addq	$2, c
	jmp For0
Fin0:
	push $'c'
	movq $FormatString2, %rdi
	pop %rsi
	movb    $0, %al
	subq $8, %rsp
	call    printf
	addq $8, %rsp
	push $':'
	movq $FormatString2, %rdi
	pop %rsi
	movb    $0, %al
	subq $8, %rsp
	call    printf
	addq $8, %rsp
	push $' '
	movq $FormatString2, %rdi
	pop %rsi
	movb    $0, %al
	subq $8, %rsp
	call    printf
	addq $8, %rsp
	push c
	movq $FormatString1Ln, %rdi    # "%llu\n"
	pop %rsi                     # The value to be displayed
	movb    $0, %al
	subq $8, %rsp
	call    printf
	addq $8, %rsp
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
