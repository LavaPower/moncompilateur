//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {UNSIGNED_INT, BOOLEAN, DOUBLE_T, CHAR_T};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

string TYPESToString(TYPES t) {
	switch(t) {
		case UNSIGNED_INT: return "entier non signé";
		case BOOLEAN: return "booléen";
		case DOUBLE_T: return "double";
		case CHAR_T: return "char";
		default: return "inconnu";
	}
}

void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
enum TYPES Identifier(){
	string variable = lexer->YYText();
	if(!IsDeclared(lexer->YYText()))
		Error("variable "+variable+" non déclarée");
	cout << "\tpush "<<variable<<endl;
	current=(TOKEN) lexer->yylex();
	return DeclaredVariables[variable];
}

enum TYPES Number(){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}

enum TYPES Char(){
	cout <<"\tpush $"<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR_T;
}

enum TYPES Double(){
	double d=atof(lexer->YYText());      // à l'adresse &f, il y a 8 octets qui contiennent le codage en norme IEEE754 du flottant 123.4
	long long unsigned int *i;     // i est un pointeur sur un espace de 8 octets sensé contenir un entier non signé en binaire naturel

	i= (long long unsigned int *) &d; // Maintenant, *i est un entier non signé sur 64 bits qui contient l'interprétation entière du codage IEEE754 du flottant 123.4 
	cout << "\tmovq $"<<*i<<", %rax"<<endl;
	cout << "\tpush %rax\t# empile le flottant "<<d<<endl;

	current=(TOKEN) lexer->yylex();
	return DOUBLE_T;
}

enum TYPES Expression();			// Called by Term() and calls Term()

enum TYPES Factor(){
	TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	    else if(current==ID)
			type = Identifier();
	    else if(current==DOUBLE)
			type = Double();
	    else if(current==CHAR)
			type = Char();
		else
			Error("'(' ou chiffre ou lettre attendue");
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(){
	OPMUL mulop;
	TYPES type = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		TYPES recu = Factor();
		if(recu != type)
			Error("type recu : "+TYPESToString(recu)+", type voulu : "+TYPESToString(type));
		switch(mulop){
			case AND:
				if(type != BOOLEAN)
					Error("AND ne gère pas ce type : "+TYPESToString(type));
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else if(type == DOUBLE_T) {
					cout << "\tfldl 8(%rsp)" << endl;
					cout << "\tfldl (%rsp)" << endl;
					cout << "\tfmulp %st(0), %st(1)" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp" << endl;
				}
				else {
					Error("MUL ne gère pas ce type : "+TYPESToString(type));
				}
				break;
			case DIV:
				if(type == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else if(type == DOUBLE_T) {
					cout << "\tfldl 8(%rsp)" << endl;
					cout << "\tfldl (%rsp)" << endl;
					cout << "\tfdivp %st(0), %st(1)" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp" << endl;
				}
				else {
					Error("MUL ne gère pas ce type : "+TYPESToString(type));
				}
				break;
			case MOD:
				if(type != UNSIGNED_INT)
					Error("MOD ne gère pas ce type : "+TYPESToString(type));
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(){
	OPADD adop;
	TYPES type = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		TYPES recu = Term();
		if(recu != type)
			Error("type recu : "+TYPESToString(recu)+", type voulu : "+TYPESToString(type));
		switch(adop){
			case OR:
				if(type != BOOLEAN)
					Error("OR ne gère pas ce type : "+TYPESToString(type));
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result	
				break;			
			case ADD:
				if(type == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq %rbx, %rax"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else if(type == DOUBLE_T) {
					cout << "\tfldl 8(%rsp)" << endl;
					cout << "\tfldl (%rsp)" << endl;
					cout << "\tfaddp %st(0), %st(1)" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp" << endl;
				}
				else {
					Error("ADD ne gère pas ce type : "+TYPESToString(type));
				}
				break;
			case SUB:
				if(type == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq %rbx, %rax"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else if(type == DOUBLE_T) {
					cout << "\tfldl 8(%rsp)" << endl;
					cout << "\tfldl (%rsp)" << endl;
					cout << "\tfsubp %st(0), %st(1)" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp" << endl;
				}
				else {
					Error("SUB ne gère pas ce type : "+TYPESToString(type));
				}
				break;
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type;
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	OPREL oprel;
	TYPES type = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		TYPES recu = SimpleExpression();
		if(recu !=type)
			Error("type recu : "+TYPESToString(recu)+", type voulu : "+TYPESToString(type));
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	TYPES recu = Expression();
	if(recu != DeclaredVariables[variable])
		Error("type recu : "+TYPESToString(recu)+", type voulu : "+TYPESToString(DeclaredVariables[variable]));
	cout << "\tpop "<<variable<<endl;
}

void Statement();

//ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
void ForStatement() {
	unsigned long tag = TagNumber++;
	current = (TOKEN) lexer->yylex();
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<string(lexer->YYText())<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	AssignementStatement();
	if(strcmp(lexer->YYText(), "TO") != 0 && strcmp(lexer->YYText(), "DOWNTO") != 0) {
		Error("TO/DOWNTO attendu");
	}
	string type_for= lexer->YYText();
	cout << "For" << tag << ":" << endl;
	current = (TOKEN) lexer->yylex();
	Expression();
	cout << "\tpop %rdx" << endl;
	cout << "\tcmp "<<variable<<", %rdx" << endl;
	if(type_for == "TO")
		cout << "\tjb Fin" << tag << endl;
	else
		cout << "\tja Fin" << tag << endl;
	int nb = 1;
	if(strcmp(lexer->YYText(), "STEP") == 0) {
		current = (TOKEN) lexer->yylex();
		if(current != NUMBER)
			Error("entier attendu.");
		nb = atoi(lexer->YYText());
		current = (TOKEN) lexer->yylex();
	}

	if(strcmp(lexer->YYText(), "DO") != 0) {
		Error("DO attendu");
	}
	current = (TOKEN) lexer->yylex();
	Statement();
	if(type_for == "TO")
		cout << "\taddq	$" << nb << ", "<< variable << endl;
	else
		cout << "\tsubq $" << nb << ", "<<variable << endl;
	
	cout << "\tjmp For" << tag << endl;
	cout << "Fin" << tag << ":" << endl;
}

//WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement() {
	unsigned long tag = TagNumber++;
	cout << "While" << tag << ":" << endl;
	current = (TOKEN) lexer->yylex();
	TYPES recu = Expression();
	if(recu != BOOLEAN)
		Error("type recu : "+TYPESToString(recu)+", type voulu : "+TYPESToString(BOOLEAN));
	cout << "\tpop %rax" << endl;
	cout << "\tcmp $0, %rax" << endl;
	cout << "\tje Fin" << tag << endl;
	if(current != KEYWORDS || strcmp(lexer->YYText(), "DO") != 0) 
		Error("DO attendu.");
	current = (TOKEN) lexer->yylex();
	Statement();
	cout << "\tjmp While" << tag << endl;
	cout << "Fin" << tag << ":" << endl;
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement() {
	current = (TOKEN) lexer->yylex();
	Statement();
	while(current == SEMICOLON) {
		current = (TOKEN) lexer->yylex();
		Statement();
	}
	
	if(current != KEYWORDS || strcmp(lexer->YYText(), "END") != 0) {
		Error("END attendu");
	}
	current = (TOKEN) lexer->yylex();
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ] 
void IfStatement() {
	unsigned long tag = TagNumber++;
	current = (TOKEN) lexer->yylex();
	TYPES recu = Expression();
	if(recu != BOOLEAN)
		Error("type recu : "+TYPESToString(recu)+", type voulu : "+TYPESToString(BOOLEAN));
	cout << "\tpop %rbx" << endl;
	cout << "\tcmp $0, %rbx" << endl;
	cout << "\tje Else" << tag << endl;
	if(current != KEYWORDS || strcmp(lexer->YYText(), "THEN") != 0) 
		Error("THEN attendu");
	current = (TOKEN) lexer->yylex();
	Statement();
	cout << "\tjmp Fin" << tag << endl;
	cout << "Else" << tag << ":" << endl;
	if(current == KEYWORDS && strcmp(lexer->YYText(), "ELSE") == 0)
	{
		current = (TOKEN) lexer->yylex();
		Statement();
	}
	cout << "Fin" << tag << ":" << endl;
}

//DisplayStatement := "DISPLAY"|"DISPLAYLN" Expression
void DisplayStatement(bool ln) {
	current = (TOKEN) lexer->yylex();
	TYPES recu = Expression();
	unsigned long tag = TagNumber++;
	switch(recu) {
		case BOOLEAN:
			cout << "\tpop %rdx" << endl;
			cout << "\tcmpq $0, %rdx" << endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi" << endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi" << endl;
			cout << "Next"<<tag<<":"<<endl;
			break;
		case UNSIGNED_INT: 
			if(ln)
				cout << "\tmovq $FormatString1Ln, %rdi    # \"%llu\\n\"" << endl;
			else
				cout << "\tmovq $FormatString1, %rdi    # \"%llu\"" << endl;
			cout << "\tpop %rsi                     # The value to be displayed" << endl;
			cout << "\tmovb    $0, %al" << endl;
			break;
		case CHAR_T:
			if(ln)
				cout << "\tmovq $FormatString2Ln, %rdi" << endl;
			else
				cout << "\tmovq $FormatString2, %rdi" << endl;
			cout << "\tpop %rsi" << endl;
			cout << "\tmovb    $0, %al" << endl;
			break;
		case DOUBLE_T:
			if(ln)
				cout << "\tmovq $FormatString3Ln, %rdi\t# \"%lf\\n\""<<endl;
			else
				cout << "\tmovq $FormatString3, %rdi\t# \"%lf\""<<endl;
			cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
			cout << "\taddq $8, %rsp"<<endl;
			cout << "\tmovb    $1, %al" << endl;
			break;

		default:
			Error("DISPLAY ne supporte pas le type : "+TYPESToString(recu));
	}

	cout << "\tsubq $8, %rsp" << endl;
	cout << "\tcall    printf" << endl;
	cout << "\taddq $8, %rsp" << endl;
	
	
}

// Statement := AssignementStatement | BlockStatement
void Statement(){
	if(current == KEYWORDS)
	{
		if(strcmp(lexer->YYText(), "BEGIN") == 0)
			BlockStatement();
		else if(strcmp(lexer->YYText(), "IF") == 0)
			IfStatement();
		else if(strcmp(lexer->YYText(), "WHILE") == 0)
			WhileStatement();
		else if(strcmp(lexer->YYText(), "FOR") == 0)
			ForStatement();
		else if(strcmp(lexer->YYText(), "DISPLAY") == 0)
			DisplayStatement(false);
		else if(strcmp(lexer->YYText(), "DISPLAYLN") == 0)
			DisplayStatement(true);
		else
			Error("Mot clé inattendu.");
	}
	else
		AssignementStatement();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

enum TYPES Type() {
	if(strcmp(lexer->YYText(), "INT") == 0) {
		current = (TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
	else if(strcmp(lexer->YYText(), "BOOL") == 0) {
		current = (TOKEN) lexer->yylex();
		return BOOLEAN;
	}
	else if(strcmp(lexer->YYText(), "DOUBLE") == 0) {
		current = (TOKEN) lexer->yylex();
		return DOUBLE_T;
	}
	else if(strcmp(lexer->YYText(), "CHAR") == 0) {
		current = (TOKEN) lexer->yylex();
		return CHAR_T;
	}
	else
	{
		Error("type inconnu : "+string(lexer->YYText()));
	}
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration() {
	set<string> var;
	if(current != ID)
		Error("Un identificateur était attendu");
	var.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current == COMMA) {
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		var.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current != COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	TYPES type = Type();
	set<string>::iterator it = var.begin();

	while (it != var.end())
	{
		switch(type) {
			case UNSIGNED_INT: 
			case BOOLEAN:
				cout << (*it) << ":\t.quad 0";
				break;
			case DOUBLE_T:
				cout << (*it) << ":\t.double 0.0";
				break;
			case CHAR_T:
				cout << (*it) << ":\t.byte 0";
				break;
			default:
				Error("type inconnu : " + TYPESToString(type));
		}
		DeclaredVariables[(*it)] = type;
		cout << "\t# " << (*it) << " - " << TYPESToString(type) << endl;
		it++;
	}
	for(int i=0; i<var.size(); i++) {
		
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart() {
	current = (TOKEN) lexer->yylex();
	VarDeclaration();
	while(current == SEMICOLON) {
		current = (TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current != DOT) {
		Error("caractère '.' attendu");
	}
	current = (TOKEN) lexer->yylex();
}

// Program := [VarDeclarationPart] StatementPart
void Program(){
	if(current==KEYWORDS && strcmp(lexer->YYText(), "VAR") == 0)
		VarDeclarationPart();
	StatementPart();	
}

int main(){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	cout << "\t.data" << endl;
	cout << "\t.align 8" << endl;
	cout << "FormatString1Ln:\t.string \"%llu\\n\"" << endl;
	cout << "FormatString2Ln:\t.string \"%c\\n\"" << endl;
	cout << "FormatString3Ln:\t.string \"%f\\n\"" << endl;
	cout << "FormatString1:\t.string \"%llu\"" << endl;
	cout << "FormatString2:\t.string \"%c\"" << endl;
	cout << "FormatString3:\t.string \"%f\"" << endl;
	cout << "TrueString:\t.string \"True\\n\"" << endl;
	cout << "FalseString:\t.string \"False\\n\"" << endl;
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}