			# This code was produced by the CERI Compiler
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $2
	push $2
	pop %rbx
	pop %rax
	cmpq %rax, %rbx
	jne valide
	push $0
	jmp fin
valide:
	push $1
fin:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
